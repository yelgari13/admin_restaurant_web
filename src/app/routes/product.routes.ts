import { Routes } from '@angular/router';
import { ProductsIndexComponent } from '@pages/products/products-index/products-index.component';
import { ProductCreateComponent } from '@pages/products/product-create/product-create.component';
import { ProductEditComponent } from '@pages/products/product-edit/product-edit.component';

export const productRoutes: Routes = [
  {
    path: '',
    component: ProductsIndexComponent,
  },
  {
    path: 'create',
    component: ProductCreateComponent,
  },
  {
    path: 'edit/:id',
    component: ProductEditComponent,
  },
];
