import { Routes } from '@angular/router';
import { isAuthenticated } from '@commom/authPermission';
import { LoginComponent } from '@pages/auth/login/login.component';
import { LayoutComponent } from '@pages/layout/layout.component';
import { HomeComponent } from '@pages/home/home.component';
import { NotFoundComponent } from '@pages/not-found/not-found.component';

// routes childrens
import { providerRoutes } from './provider.routes';
import { productRoutes } from './product.routes';
import { employeeRoutes } from './employee.routes';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    component: LayoutComponent,
    canActivate: [isAuthenticated],
    children: [
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'providers',
        children: providerRoutes,
      },
      {
        path: 'employees',
        children: employeeRoutes,
      },
      {
        path: 'products',
        children: productRoutes,
      }
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: '**', component: NotFoundComponent },
];
