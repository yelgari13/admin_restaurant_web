import { Routes } from '@angular/router';
import { EmployeesIndexComponent } from '@pages/employees/employees-index/employees-index.component';
import { EmployeeCreateComponent } from '@pages/employees/employee-create/employee-create.component';
import { EmployeeEditComponent } from '@pages/employees/employee-edit/employee-edit.component';

export const employeeRoutes: Routes = [
  {
    path: '',
    component: EmployeesIndexComponent,
  },
  {
    path: 'create',
    component: EmployeeCreateComponent,
  },
  {
    path: 'edit/:id',
    component: EmployeeEditComponent,
  },
];