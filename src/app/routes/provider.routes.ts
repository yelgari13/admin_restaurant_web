import { Routes } from '@angular/router';
import { ProvidersIndexComponent } from '@pages/providers/providers-index/providers-index.component';
import { ProviderCreateComponent } from '@pages/providers/provider-create/provider-create.component';
import { ProviderEditComponent } from '@pages/providers/provider-edit/provider-edit.component';

export const providerRoutes: Routes = [
  {
    path: '',
    component: ProvidersIndexComponent,
  },
  {
    path: 'create',
    component: ProviderCreateComponent,
  },
  {
    path: 'edit/:id',
    component: ProviderEditComponent,
  },
];
