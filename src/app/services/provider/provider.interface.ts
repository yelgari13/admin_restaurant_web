export interface IProvider {
  restaurantId: string;
  providerId: string;
  providerBussinessName: string;
  providerRuc: string;
  providerPhone: string;
  providerEmail: string;
  country: string;
  departament: string;
  province: string;
  district: string;
  address: string;
  addressReference: string;
  IsDelete: boolean;
}

export interface IProviderCreate {
  restaurantId: string;
  providerBussinessName: string;
  providerRuc: string;
  providerPhone: string;
  providerEmail: string;
  country: string;
  departament: string;
  province: string;
  district: string;
  address: string;
  addressReference: string;
}
