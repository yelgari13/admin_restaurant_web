import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { headers$ } from '@shared/audit';
import { environment } from '@environments/environment';
import { catchError, map, of } from 'rxjs';

import { IProvider, IProviderCreate } from './provider.interface';

interface IFilter {
  providerBussinessName: string;
}

@Injectable({
  providedIn: 'root',
})
export class ProviderService {
  private http = inject(HttpClient);
  private baseUrl: string;
  private requestOptions: Object;

  constructor() {
    this.baseUrl = `${environment.API_RESTAURANT_PATH}/providers`;
    this.requestOptions = {
      headers: headers$.getValue(),
    };
  }

  paginate(restaurantId: string | undefined, filter: IFilter | null) {
    const conditions: string[] = [];

    if (filter?.providerBussinessName) {
      conditions.push(`providerBussinessName=${filter?.providerBussinessName}`);
    }

    return this.http
      .get(
        `${this.baseUrl}/${restaurantId}/search?${conditions.join('&')}`,
        this.requestOptions
      )
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  getById(providerId: string) {
    return this.http
      .get(`${this.baseUrl}/${providerId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  create(data: IProviderCreate) {
    return this.http
      .post(`${this.baseUrl}/create`, data, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  update(providerId: string, data: IProvider) {
    const payload = {
      providerPhone: data.providerPhone,
      providerEmail: data.providerEmail,
      country: data.country,
      departament: data.departament,
      province: data.province,
      district: data.district,
      address: data.address,
      addressReference: data.addressReference,
    };

    return this.http
      .put(`${this.baseUrl}/update/${providerId}`, payload, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  delete(providerId: string) {
    return this.http
      .delete(`${this.baseUrl}/delete/${providerId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }
}
