import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { headers$ } from '@shared/audit';
import { environment } from '@environments/environment';
import { catchError, map, of } from 'rxjs';

import { IProduct, IProductCreate } from './product.interface';

interface IFilter {
  productName: string;
}

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private http = inject(HttpClient);
  private baseUrl: string;
  private requestOptions: Object;

  constructor() {
    this.baseUrl = `${environment.API_RESTAURANT_PATH}/products`;
    this.requestOptions = {
      headers: headers$.getValue(),
    };
  }

  paginate(restaurantId: string | undefined, filter: IFilter | null) {
    const conditions: string[] = [];

    if (filter?.productName) {
      conditions.push(`productName=${filter?.productName}`);
    }

    return this.http
      .get(
        `${this.baseUrl}/${restaurantId}/search?${conditions.join('&')}`,
        this.requestOptions
      )
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  getById(productId: string) {
    return this.http
      .get(`${this.baseUrl}/${productId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  create(data: IProductCreate) {
    return this.http
      .post(`${this.baseUrl}/create`, data, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  update(productId: string, data: IProduct) {
    const payload = {
      productName: data.productName,
      productType: data.productType,
      productBrand: data.productBrand,
      productDescription: data.productDescription,
      productStock: data.productStock,
      productPrice: data.productPrice,
      productDiscount: data.productDiscount,
      currencySymbol: data.currencySymbol,
    };

    return this.http
      .put(`${this.baseUrl}/update/${productId}`, payload, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }

  delete(productId: string) {
    return this.http
      .delete(`${this.baseUrl}/delete/${productId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }
}

