export interface IProduct {
  restaurantId: string;
  productId: string;
  productName: string;
  productType: string;
  productBrand: string;
  productDescription: string;
  productStock: number;
  productPrice: number;
  productDiscount: number;
  currencySymbol: string;
  IsDelete: boolean;
}

export interface IProductCreate {
  restaurantId: string;
  productName: string;
  productType: string;
  productBrand: string;
  productDescription: string;
  productStock: number;
  productPrice: number;
  productDiscount: number;
  currencySymbol: string;
}

