export interface IEmployee {
    employeeId: string;
    restaurantId: string;
    adminId: string;
    employeeName: string;
    employeeSurname: string;
    employeeBirthday: Date;
    employeePhone: string;
    employeeEmail: string;
    employeeType: string;
    contractType: string;
    address: string;
    startedWork: Date;
    endedWork: Date;
    employeeSalary: string;
}

export interface IEmployeeCreate {
    restaurantId: string;
    adminId: string;
    employeeName: string;
    employeeSurname: string;
    employeeBirthday: Date;
    employeePhone: string;
    employeeEmail: string;
    employeeType: string;
    contractType: string;
    address: string;
    startedWork: Date;
    endedWork: Date;
    employeeSalary: string;
}
