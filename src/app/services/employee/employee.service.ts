import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, of } from 'rxjs';

import { headers$ } from '@shared/audit';
import { environment } from '@environments/environment';
import { IEmployee, IEmployeeCreate } from './employee.interface';

interface IFilter {
  employeeName: string;
}

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  constructor(private http: HttpClient) {}

  private baseUrl: string = `${environment.API_RESTAURANT_PATH}/employees`;
  private requestOptions: Object = {
    headers: headers$.getValue(),
  };

  paginate(restaurantId: string | undefined, filter: IFilter | null) {
    const conditions: string[] = [];

    if (filter?.employeeName) {
      conditions.push(`employeeName=${filter?.employeeName}`);
    }

    return this.http
      .get(`${this.baseUrl}/${restaurantId}/search?${conditions.join('&')}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => of(error))
      );
  }

  getById(employeeId: string) {
    return this.http
      .get(`${this.baseUrl}/${employeeId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => of(error))
      );
  }

  create(data: IEmployeeCreate) {
    return this.http
      .post(`${this.baseUrl}/create`, data, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => of(error))
      );
  }

  update(employeeId: string, data: IEmployee) {
    const payload = {
      employeeName: data.employeeName,
      employeeSurname: data.employeeSurname,
      employeeBirthday: data.employeeBirthday,
      employeePhone: data.employeePhone,
      employeeEmail: data.employeeEmail,
      employeeType: data.employeeType,
      contractType: data.contractType,
      address: data.address,
      startedWork: data.startedWork,
      endedWork: data.endedWork,
      employeeSalary: data.employeeSalary,
    };

    return this.http
      .put(`${this.baseUrl}/update/${employeeId}`, payload, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => of(error))
      );
  }

  delete(employeeId: string) {
    return this.http
      .delete(`${this.baseUrl}/delete/${employeeId}`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => of(error))
      );
  }
}
