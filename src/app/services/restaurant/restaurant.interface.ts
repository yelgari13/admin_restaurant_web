export interface IRestaurant {
  restaurantId: string;
  restaurantRuc: string;
  restaurantName: string;
  restaurantLogoUrl: string;
  currencyId: string;
  country: string;
  departament: string;
  province: string;
  district: string;
  address: string;
  addressRefence: string;
  isDelete: boolean;
}
