import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { headers$ } from '@shared/audit';
import { environment } from '@environments/environment';
import { catchError, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RestaurantService {
  private http = inject(HttpClient);
  private baseUrl: string;
  private requestOptions: Object;

  constructor() {
    this.baseUrl = `${environment.API_RESTAURANT_PATH}/restaurants`;
    this.requestOptions = {
      headers: headers$.getValue(),
    };
  }

  getRestaurantByAdmin() {
    return this.http
      .get(`${this.baseUrl}/admin`, this.requestOptions)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }
}
