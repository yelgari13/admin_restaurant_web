import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { catchError, map, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private http = inject(HttpClient);
  private baseUrl: string;

  constructor() {
    this.baseUrl = `${environment.API_RESTAURANT_PATH}/auth`;
  }

  login(usename: string, password: string) {
    return this.http
      .post(`${this.baseUrl}/login/${usename}/${password}`, null)
      .pipe(
        map((response: any) => response),
        catchError((error) => {
          return of(error);
        })
      );
  }
}
