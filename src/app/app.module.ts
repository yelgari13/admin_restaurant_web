import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProvidersIndexComponent } from './pages/providers/providers-index/providers-index.component';
import { ProductsIndexComponent } from './pages/products/products-index/products-index.component'; 
import { LoginComponent } from './pages/auth/login/login.component';
import { ErrorMessagesComponent } from './components/error-messages/error-messages.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutComponent } from './pages/layout/layout.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProviderCreateComponent } from './pages/providers/provider-create/provider-create.component';
import { ProductCreateComponent } from './pages/products/product-create/product-create.component'; 
import { ProviderFormComponent } from './pages/providers/provider-form/provider-form.component';
import { ProductFormComponent } from './pages/products/product-form/product-form.component'; 
import { ProviderEditComponent } from './pages/providers/provider-edit/provider-edit.component';
import { ProductEditComponent } from './pages/products/product-edit/product-edit.component'; 
import { EmployeesIndexComponent } from './pages/employees/employees-index/employees-index.component';
import { EmployeeFormComponent } from './pages/employees/employee-form/employee-form.component';
import { EmployeeEditComponent } from './pages/employees/employee-edit/employee-edit.component';
import { EmployeeCreateComponent } from './pages/employees/employee-create/employee-create.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProvidersIndexComponent,
    ProductsIndexComponent,
    LoginComponent,
    ErrorMessagesComponent,
    NavbarComponent,
    LayoutComponent,
    NotFoundComponent,
    FooterComponent,
    ProviderCreateComponent,
    ProductCreateComponent,
    ProductEditComponent,
    ProviderFormComponent,
    ProductFormComponent,
    ProviderEditComponent,
    EmployeesIndexComponent,
    EmployeeFormComponent,
    EmployeeEditComponent,
    EmployeeCreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
