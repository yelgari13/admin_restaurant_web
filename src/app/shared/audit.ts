import { BehaviorSubject } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

interface IHeaders {
  [header: string]: string | string[];
}

export const token$ = new BehaviorSubject<string>('');
export const headers$ = new BehaviorSubject<HttpHeaders | IHeaders>({
  'Content-Type': '',
  Authorization: '',
});
