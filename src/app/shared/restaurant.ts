import { BehaviorSubject } from 'rxjs';
import { IRestaurant } from '@services/restaurant/restaurant.interface';

export const restaurant$ = new BehaviorSubject<IRestaurant | null>(null);
