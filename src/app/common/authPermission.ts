import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { token$, headers$ } from '@shared/audit';

export const isAuthenticated = () => {
  const router = inject(Router);
  const token = localStorage.getItem('token_admin');

  if (token) {
    token$.next(String(token));
    headers$.next({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });
    return true;
  } else {
    router.navigate(['/login']);
    return false;
  }
};
