import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { IRestaurant } from '@services/restaurant/restaurant.interface';
import { restaurant$ } from '@shared/restaurant';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  restaurant: IRestaurant | null = null;
  router = inject(Router);

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();
  }

  logout() {
    localStorage.removeItem('token_admin');
    this.router.navigate(['/login']);
  }
}
