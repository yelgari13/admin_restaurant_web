import { Component, inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '@services/employee/employee.service';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss'],
})
export class EmployeeFormComponent {
  @Input() isEdit: boolean;

  private activatedRoute = inject(ActivatedRoute);
  private employeeId: string;

  form: FormGroup;
  employeeService = inject(EmployeeService);
  router = inject(Router);
  restaurant: IRestaurant | null;
  isLoading: boolean;

  constructor(fb: FormBuilder) {
    this.isEdit = false;
    this.employeeId = '';
    this.restaurant = null;
    this.isLoading = false;

    this.form = fb.group({
      employeeName: ['', Validators.required],
      employeeSurname: [''],
      employeeBirthday: [''],
      employeePhone: [''],
      employeeEmail: [''],
      employeeType: [''],
      contractType: [''],
      address: [''],
      startedWork: [''],
      endedWork: [''],
      employeeSalary: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();

    if (this.isEdit) {
      this.form.get('employeeName')?.disable();
      this.form.get('employeeSurname')?.disable();

      this.activatedRoute?.params.subscribe((params) => {
        const id = params['id'];
        this.employeeId = id;
        this.getEmployeeById(id);
      });
    }
  }

  getEmployeeById(id: string) {
    this.employeeService.getById(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        this.form.patchValue(value.data);
      },
    });
  }

  onSubmit() {
    if (!this.isEdit) {
      this.isCreate();
    } else {
      this.isUpdate();
    }
  }

  isCreate() {
    this.isLoading = true;

    const payload = this.form.value;
    payload.restaurantId = this.restaurant?.restaurantId;

    this.employeeService.create(payload).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/employees']);
      },
    });
  }

  isUpdate() {
    this.isLoading = true;

    this.employeeService.update(this.employeeId, this.form.value).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/employees']);
      },
    });
  }
}
