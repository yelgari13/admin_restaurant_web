import { Component, inject } from '@angular/core';
import { EmployeeService } from '@services/employee/employee.service';
import { IEmployee } from '@services/employee/employee.interface';
import { FormBuilder, FormGroup } from '@angular/forms';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';

@Component({
  selector: 'app-employees-index',
  templateUrl: './employees-index.component.html',
  styleUrls: ['./employees-index.component.scss'],
})
export class EmployeesIndexComponent {
  employeeService = inject(EmployeeService);
  employees: IEmployee[] | null;
  restaurant: IRestaurant | null = null;
  filter: FormGroup;

  tableColumns = [
    'Nombre',
    'Apellido',
    'Fecha de nacimiento',
    'Teléfono',
    'Correo',
    'Tipo de empleado',
    'Tipo de contrato',
    'Dirección',
    'Fecha de inicio',
    'Fecha de finalización',
    'Salario',
    'Acciones',
  ];

  constructor(fb: FormBuilder) {
    this.employees = null;

    this.filter = fb.group({
      employeeName: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();
    this.search();
  }

  clearSearch() {
    this.filter.reset();
    this.search();
  }

  search() {
    this.employeeService
      .paginate(this.restaurant?.restaurantId, this.filter.value)
      .subscribe({
        next: (value) => {
          if (value?.error) {
            console.error('Error fetching employees:', value.error);
            return;
          }
  
          console.log('Employees data:', value.data); // Imprimir datos en la consola
          this.employees = value.data;
        },
        error: (err) => {
          console.error('Error fetching employees:', err);
        }
      });
  }
  deleteItem(id: string) {
    this.employeeService.delete(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        console.log({ value });
      },

      complete: () => {
        this.search();
      },
    });
  }
}
