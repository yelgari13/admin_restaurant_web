import { Component, inject } from '@angular/core';
import { RestaurantService } from '@services/restaurant/restaurant.service';
import { restaurant$ } from '@shared/restaurant';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent {
  restaurantService = inject(RestaurantService);
  isLoading: boolean = true;

  constructor() {}

  ngOnInit(): void {
    this.restaurantService.getRestaurantByAdmin().subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        restaurant$.next(value.data);
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }
}
