import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsIndexComponent } from './products-index.component'; // Modificado el nombre del componente

describe('ProductIndexComponent', () => { // Modificado el nombre del componente en la descripción
  let component: ProductsIndexComponent; // Modificado el nombre del componente
  let fixture: ComponentFixture<ProductsIndexComponent>; // Modificado el nombre del componente

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProductsIndexComponent] // Modificado el nombre del componente
    });
    fixture = TestBed.createComponent(ProductsIndexComponent); // Modificado el nombre del componente
    component = fixture.componentInstance; // Modificado el nombre del componente
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
