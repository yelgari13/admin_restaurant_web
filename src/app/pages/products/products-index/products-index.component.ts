import { Component, inject } from '@angular/core';
import { ProductService } from '@services/product/product.service';
import { IProduct } from '@services/product/product.interface';
import { FormBuilder, FormGroup } from '@angular/forms';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';

@Component({
  selector: 'app-products-index',
  templateUrl: './products-index.component.html',
  styleUrls: ['./products-index.component.scss'],
})
export class ProductsIndexComponent {
  productService = inject(ProductService);
  products: IProduct[] | null;
  restaurant: IRestaurant | null = null;
  filter: FormGroup;

  tableColumns = [
    'Product',
    'Tipo',
    'Marca',
    'Descripción',
    'Stock',
    'Precio',
    'Descuento',
    'Símbolo de Moneda',
    'Acciones',
  ];

  constructor(fb: FormBuilder) {
    this.products = null;

    this.filter = fb.group({
      productName: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();
    this.search();
  }

  clearSearch() {
    this.filter.reset();
    this.search();
  }

  search() {
    this.productService
      .paginate(this.restaurant?.restaurantId, this.filter.value)
      .subscribe({
        next: (value) => {
          if (value?.error) {
            return;
          }

          this.products = value.data;
        },
      });
  }

  deleteItem(id: string) {
    this.productService.delete(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        console.log({ value });
      },

      complete: () => {
        this.search();
      },
    });
  }
}

