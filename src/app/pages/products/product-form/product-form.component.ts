import { Component, inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@services/product/product.service';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent {
  @Input() isEdit: boolean;

  private activatedRoute = inject(ActivatedRoute);
  private productId: string;

  form: FormGroup;
  productService = inject(ProductService);
  router = inject(Router);
  restaurant: IRestaurant | null;
  isLoading: boolean;

  constructor(fb: FormBuilder) {
    this.isEdit = false;
    this.productId = '';
    this.restaurant = null;
    this.isLoading = false;

    this.form = fb.group({
      productName: ['', Validators.required],
      productImageUrl: [''],
      productType: [''],
      productBrand: [''],
      productDescription: [''],
      productStock: [''],
      productPrice: [''],
      productDiscount: [''],
      currencySymbol: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();

    if (this.isEdit) {
      // Deshabilitar campos si es edición
      this.form.get('productName')?.disable();
      this.form.get('productImageUrl')?.disable();

      this.activatedRoute?.params.subscribe((params) => {
        const id = params['id'];
        this.productId = id;
        this.getProductById(id);
      });
    }
  }

  getProductById(id: string) {
    this.productService.getById(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        this.form.patchValue(value.data);
      },
    });
  }

  onSubmit() {
    if (!this.isEdit) {
      this.createProduct();
    } else {
      this.updateProduct();
    }
  }

  createProduct() {
    this.isLoading = true;

    const payload = this.form.value;
    payload.restaurantId = this.restaurant?.restaurantId;

    this.productService.create(payload).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/products']);
      },
    });
  }

  updateProduct() {
    this.isLoading = true;

    this.productService.update(this.productId, this.form.value).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/products']);
      },
    });
  }
}
