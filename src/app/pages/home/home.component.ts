import { Component } from '@angular/core';
import { IRestaurant } from '@services/restaurant/restaurant.interface';
import { restaurant$ } from '@shared/restaurant';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  restaurant: IRestaurant | null = null;

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();
  }
}
