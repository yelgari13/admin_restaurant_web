import { Component, inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  form: FormGroup;
  authService = inject(AuthService);
  router = inject(Router);
  isLoading: Boolean = false;

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  async onSubmit() {
    if (this.form.invalid) {
      console.error('Campos inválidos del formulario');
      return;
    }

    this.isLoading = true;

    const { username, password } = this.form.value;

    this.authService.login(username, password).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }
        localStorage.setItem('token_admin', value.data.token);
        this.router.navigate(['/home']);
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }
}
