import { Component, inject, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderService } from '@services/provider/provider.service';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-provider-form',
  templateUrl: './provider-form.component.html',
  styleUrls: ['./provider-form.component.scss'],
})
export class ProviderFormComponent {
  @Input() isEdit: boolean;

  private activatedRoute = inject(ActivatedRoute);
  private providerId: string;

  form: FormGroup;
  providerService = inject(ProviderService);
  router = inject(Router);
  restaurant: IRestaurant | null;
  isLoading: boolean;

  constructor(fb: FormBuilder) {
    this.isEdit = false;
    this.providerId = '';
    this.restaurant = null;
    this.isLoading = false;

    // form = new FormGroup({
    //   first: new FormControl({value: 'Nancy', disabled: true}, Validators.required),
    //   last: new FormControl('Drew', Validators.required)
    // });
    this.form = fb.group({
      providerBussinessName: ['', Validators.required],
      providerRuc: [''],
      providerPhone: [''],
      providerEmail: [''],
      country: [''],
      departament: [''],
      province: [''],
      district: [''],
      address: [''],
      addressReference: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();

    if (this.isEdit) {
      this.form.get('providerBussinessName')?.disable();
      this.form.get('providerRuc')?.disable();

      this.activatedRoute?.params.subscribe((params) => {
        const id = params['id'];
        this.providerId = id;
        this.getProviderById(id);
      });
    }
  }

  getProviderById(id: string) {
    this.providerService.getById(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        this.form.patchValue(value.data);
      },
    });
  }

  onSubmit() {
    if (!this.isEdit) {
      this.isCreate();
    } else {
      this.isUpdate();
    }
  }

  isCreate() {
    this.isLoading = true;

    const payload = this.form.value;
    payload.restaurantId = this.restaurant?.restaurantId;

    this.providerService.create(payload).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/providers']);
      },
    });
  }

  isUpdate() {
    this.isLoading = true;

    this.providerService.update(this.providerId, this.form.value).subscribe({
      next: (value) => {
        console.log(value);
      },
      complete: () => {
        this.router.navigate(['/home/providers']);
      },
    });
  }
}
