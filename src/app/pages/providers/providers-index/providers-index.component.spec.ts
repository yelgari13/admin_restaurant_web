import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvidersIndexComponent } from './providers-index.component';

describe('ProvidersIndexComponent', () => {
  let component: ProvidersIndexComponent;
  let fixture: ComponentFixture<ProvidersIndexComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProvidersIndexComponent]
    });
    fixture = TestBed.createComponent(ProvidersIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
