import { Component, inject } from '@angular/core';
import { ProviderService } from '@services/provider/provider.service';
import { IProvider } from '@services/provider/provider.interface';
import { FormBuilder, FormGroup } from '@angular/forms';

import { restaurant$ } from '@shared/restaurant';
import { IRestaurant } from '@services/restaurant/restaurant.interface';

@Component({
  selector: 'app-providers-index',
  templateUrl: './providers-index.component.html',
  styleUrls: ['./providers-index.component.scss'],
})
export class ProvidersIndexComponent {
  providerService = inject(ProviderService);
  providers: IProvider[] | null;
  restaurant: IRestaurant | null = null;
  filter: FormGroup;

  tableColumns = [
    'Proveedor',
    'Ruc',
    'Teléfono',
    'Correo',
    'País',
    'Departamento',
    'Provincia',
    'Distrito',
    'Dirección',
    'Referencia',
    'Acciones',
  ];

  constructor(fb: FormBuilder) {
    this.providers = null;

    this.filter = fb.group({
      providerBussinessName: [''],
    });
  }

  ngOnInit(): void {
    this.restaurant = restaurant$.getValue();
    this.search();
  }

  clearSearch() {
    this.filter.reset();
    this.search();
  }

  search() {
    this.providerService
      .paginate(this.restaurant?.restaurantId, this.filter.value)
      .subscribe({
        next: (value) => {
          if (value?.error) {
            return;
          }

          this.providers = value.data;
        },
      });
  }

  deleteItem(id: string) {
    this.providerService.delete(id).subscribe({
      next: (value) => {
        if (value?.error) {
          return;
        }

        console.log({ value });
      },

      complete: () => {
        this.search();
      },
    });
  }
}
